﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPCDestroy : MonoBehaviour {


	// Use this for initialization
	void OnCollisionEnter( Collision collision)
    {
        if(collision.collider.GetComponent<Collider>().tag != "Bullet")
        {
            return;
        }
        else
        {
            Destroy(gameObject);
        }
    }
}
