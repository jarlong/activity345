﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class NPCMovement : MonoBehaviour {

    public Transform target;
    public GameObject[] goals;
    public float speed;
    public int goalInd;
    // Use this for initialization

    NavMeshAgent agent;
	void Start ()
    {
        target = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
        goals = GameObject.FindGameObjectsWithTag("Goal");//.GetComponent<Transform>();
        goalInd = Random.Range(0, goals.Length);
        agent = GetComponent<NavMeshAgent>();
        //Patrol();
        speed = 1;
	}

    void Update()
    {
       Patrol();
    }
    void Patrol()
    {
        if(Vector3.Distance(this.transform.position,target.transform.position) >=4)
        {
            agent.SetDestination(target.transform.position);
        }
       // else if(Vector3.Distance (this.transform.position, goals[goalInd].transform.position) <= 4)
        //{
      //      goalInd = Random.Range(0, goals.Length);
      //  }
        else
        {

        }
    }
    /*
    void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<Collider>().tag != "Player")
        {
            return;
        }
        float step = speed * Time.deltaTime;
        transform.position = Vector3.MoveTowards(transform.position, target.position, step);
    }
    void OnTriggerStay(Collider other)
    {
        if (other.GetComponent<Collider>().tag != "Player")
        {
            return;
        }
        float step = speed * Time.deltaTime;
        transform.position = Vector3.MoveTowards(transform.position, target.position, step);
    }*/

}
