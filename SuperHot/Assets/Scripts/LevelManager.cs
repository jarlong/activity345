﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class LevelManager : MonoBehaviour
{
	public GameObject npcPrefab;
	public Transform floor;
    public int spawnRoomNum = 0;

	private List<GameObject> npcs = new List<GameObject>();
	private const int NO_OF_NPCs = 6;
	private const int MAX_ATTEMPTS = 10000;
	private const string NPC_TAG = "NPC";
	private const string PLAYER_TAG = "Player";

	// Use this for initialization
	void Start ()
    {
		List<Transform> tiles = floor.GetComponentsInChildren<Transform> ().OfType<Transform>().ToList();
		for (int i = 0; npcs.Count < NO_OF_NPCs && i < MAX_ATTEMPTS && tiles.Count > 0; i++) {
			// find a potential position
			int randomIndex = Random.Range (0, tiles.Count);
			Vector3 placement = tiles[randomIndex].position;
			tiles.RemoveAt(randomIndex);
			bool good_placement = true;
			foreach (GameObject npc in npcs)
            {
				RaycastHit hit;
				Vector3 origin = placement + Vector3.up * 1.5f;
				Vector3 target = npc.transform.position + Vector3.up * 1.5f;
                if(spawnRoomNum < 3)
                {
                    if (Physics.Raycast(origin, target - origin, out hit, 100))
                    {
                        Debug.Log(hit.transform.name);
                        if (hit.transform.tag != PLAYER_TAG)
                        {
                            good_placement = false;
                            Debug.Log("Opps, we are not in the same room!");
                        }
                    }
                }
				else if(spawnRoomNum >=3)
                {
                    if (Physics.Raycast(origin, target - origin, out hit, 100))
                    {
                        Debug.Log(hit.transform.name);
                        if (hit.transform.tag == NPC_TAG || hit.transform.tag == PLAYER_TAG)
                        {
                            good_placement = false;
                            Debug.Log("Opps, we are in the same room!");
                        }
                    }
                }
                
			}	
			if (good_placement)
            {
				npcs.Add (Instantiate (npcPrefab, placement, Quaternion.identity));
				Debug.Log ("POSITION:" + placement);
                spawnRoomNum++;
			}
			
		}
			
	}
	
	// Update is called once per frame
	void Update ()
    {
		
	}
}
