﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RaycastShoot : MonoBehaviour {

    public int gunDamage = 1;
    public float fireRate = 0.25f;
    public float weaponRange = 50f;
    public float hitForce = 100f;
    public Transform gunEnd;
    //public Rigidbody bullet;

    private Camera fpsCam;
    private WaitForSeconds shotDuration = new WaitForSeconds(0.07f);
    private AudioSource gunAudio;
    //private LineRenderer laserLine;
    private float nextFire;
    private Rigidbody bullet;
	
	void Start ()
    {
        //laserLine = GetComponent<LineRenderer>();
        gunAudio = GetComponent<AudioSource>();
        fpsCam = GetComponentInParent<Camera>();
        bullet = GameObject.FindGameObjectWithTag("Bullet").GetComponent<Rigidbody>();
	}
	
	void Update ()
    {
		if(Input.GetButtonDown("Fire1") && Time.time > nextFire)
        {
            nextFire = Time.time + fireRate;
            StartCoroutine(ShotEffect());

            Vector3 rayOrigin = fpsCam.ViewportToWorldPoint(new Vector3(0.5f, 0.5f, 0));
            RaycastHit hit;

            // laserLine.SetPosition(0, gunEnd.position);
            Rigidbody firedBullet;

            if (Physics.Raycast(rayOrigin,fpsCam.transform.forward, out hit, weaponRange))
            {
                
                firedBullet = Instantiate(bullet, gunEnd.position, gunEnd.rotation) as Rigidbody;
                firedBullet.AddForce(gunEnd.forward * hitForce *TimeManager.GetInstance().myTimeScale);

               // Destroy(firedBullet, 1.5f);
                //laserLine.SetPosition(1, hit.point);
            }
            else
            {
              //  laserLine.SetPosition(1, rayOrigin + (fpsCam.transform.forward * weaponRange));
            }
        }
	}
    private IEnumerator ShotEffect()
    {
        gunAudio.Play();

       // laserLine.enabled = true;

        yield return shotDuration;
        //laserLine.enabled = false;
    }
}
